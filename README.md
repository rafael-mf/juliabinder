# Julia Binder demo

This is a demo of Julia functionality for the Binder project. Simply
go to the URL below and it will launch an interactive Julia environment:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/rafael-mf%2Fjuliabinder/master)

Example obtaned from a [github repository](https://github.com/binder-examples/demo-julia).